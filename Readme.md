# Web Seed Project

This is a seed project for an [AngularJS][]/[Bootstrap][] site using [Express][]. It has authentication through
[Google Plus][], and uses [trivialdb][] as its data store. It is set up with [Grunt][], [Less][] compilation, and unit
tests using [Mocha][].

[AngularJS]: https://angularjs.org/
[Bootstrap]: http://getbootstrap.com/
[Express]: http://expressjs.com/
[Google Plus]: https://developers.google.com/+/features/sign-in
[trivialdb]: https://github.com/Morgul/trivialdb
[Grunt]: http://gruntjs.com/
[Less]: http://lesscss.org/
[Mocha]: http://mochajs.org/


## How to use

Simply checkout this project into the folder you want your new project to be. Once that's done, install dependencies in
the `scripts` directory (`npm install`), and then run the `init.js` script _**from the root of the project**_:

```bash
cd scripts
npm install
cd ..
./scripts/init.js "Project Name" short-name
```

This will remove the `.git`, `.idea` (if any), and `scripts` directory, and replace text throughout the project so that
it's ready to go without any work on your part.