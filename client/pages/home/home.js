// ---------------------------------------------------------------------------------------------------------------------
// HomeController
//
// @module home.js
// ---------------------------------------------------------------------------------------------------------------------

function HomeController($scope)
{
} // end HomeController

// ---------------------------------------------------------------------------------------------------------------------

angular.module('web-seed.controllers').controller('HomeController', [
    '$scope',
    HomeController
]);

// ---------------------------------------------------------------------------------------------------------------------