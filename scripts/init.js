#!/usr/bin/env node

//----------------------------------------------------------------------------------------------------------------------
// Initialize the project with a new name
//
// @module init.js
//----------------------------------------------------------------------------------------------------------------------

var fs = require('fs');
var util = require('util');

var replace = require('replace');
var rimraf = require('rimraf');

var program = require('commander');

//----------------------------------------------------------------------------------------------------------------------

program
    .version('0.0.1')
    .arguments('<projectName> [shortName]')
    .action(function (projectName, shortName) {
        console.log("Initializing '%s' project...", projectName);

        // In case short name wasn't specified
        shortName = shortName || name.toLowerCase().replace(' ', '-');

        // Remove the .git directory
        rimraf.sync('.git');

        // Remove the .idea directory
        rimraf.sync('.idea');

        // Remove the scripts directory
        rimraf.sync('./Readme.md');

        // Write a new readme
        fs.writeFileSync('./Readme.md', util.format(
            '# %s Project\n\n' +
            'This is a placeholder for a real readme.',
            projectName
        ));

        // Replace project name
        replace({
            regex: "Web Seed",
            replacement: projectName,
            paths: ['.'],
            excludes: ['./node_modules'],
            recursive: true,
            silent: true
        });

        // Replace short name
        replace({
            regex: "web-seed",
            replacement: shortName,
            paths: ['.'],
            excludes: ['./node_modules'],
            recursive: true,
            silent: true
        });

        // Remove the scripts directory (This MUST be last!)
        rimraf.sync('./scripts');
    });

// Output the help if not enough arguments were specified.
if (!process.argv.slice(2).length)
{
    program.outputHelp();
} // end if

program.parse(process.argv);


//----------------------------------------------------------------------------------------------------------------------